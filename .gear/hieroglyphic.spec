%define _unpackaged_files_terminate_build 1
%define xdg_name io.github.finefindus.Hieroglyphic

Name: hieroglyphic
Version: 1.1.0
Release: alt1
Summary: LaTeX symbol finder by sketching with over 1000 symbols
License: GPL-3.0
Group: Development/Tools
Url: https://github.com/FineFindus/Hieroglyphic
Vcs: https://github.com/FineFindus/Hieroglyphic.git
BuildArch: x86_64
Source0: %name-%version.tar
Source1: crates.tar
Patch0: %name-%version-deps.patch

BuildRequires(pre): rpm-macros-meson
BuildRequires: rust
BuildRequires: cmake
BuildRequires: meson
BuildRequires: rust-cargo
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-2.0)
BuildRequires: pkgconfig(gtk4) 
BuildRequires: pkgconfig(libadwaita-1)

%description
Hieroglyphic enhances LaTeX usage by offering an innovative way
to find over 1000 symbols from various packages. By allowing users
to sketch symbols, it simplifies the search process,
making it intuitive and efficient. This standalone application,
powered by a detexify classifier port, operates entirely offline,
ensuring accessibility and convenience. Ideal for LaTeX users
seeking a streamlined symbol-finding experience.

%prep
%setup
%autopatch
mkdir -p .cargo
cat >> .cargo/config.toml <<EOF
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "crates"
EOF

%ifdef bootstrap
cargo vendor crates
tar cf %SOURCE1 crates
%else
tar xf %SOURCE1
%endif

%build
%meson
%meson_build

%install
%meson_install
%find_lang --with-gnome %name

%check
%meson_test

%files -f %name.lang
%doc README.md LICENSE
%_bindir/%name
%_datadir/applications/%xdg_name.desktop
%_datadir/glib-2.0/schemas/%xdg_name.gschema.xml
%_datadir/%name/resources.gresource
%_iconsdir/hicolor/scalable/apps/%xdg_name.svg
%_iconsdir/hicolor/symbolic/apps/%xdg_name-symbolic.svg
%_datadir/metainfo/%xdg_name.metainfo.xml

%changelog
* Tue Jul 9 2024 Aleksandr A. Voyt <sobue@altlinux.org> 1.1.0-alt1
- Initial commit
